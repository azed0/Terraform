provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "ec2" {
  ami = "ami-0fc5d935ebf8bc3bc"
  instance_type = "t2.micro"
}
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16" #plage d'IP
  tags = {
    Name= "My-VPC"
  }
}
resource "aws_subnet" "subnet" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
}