# Terrafrom
 
Aymene AZEGGAGH

Installation de terraform depuis le site officiel./ choco
----
    choco install terraform
Installation de AWS CLI
-----------------------------------------------
    choco install awscli
Création utilisateur :
-------------------------------------------
    Se connecter a Aws et créer un groupe d'utilisateur et donner les droits Admins
    Créer un utilisateur, l'ajouter au group créé précedemment
    Télcharger le csv des access key
    Avec AWS CLI on configure l'utilsateur créé avec "Aws configure", rensiegner les acces key et nous sommes connectés avec ce compte depuis le terminal de notre machine local.

Commandes Terraform :
-----------
    terraform init: Initialise le répertoire de travail Terraform.
    terraform plan: Affiche un plan des actions que Terraform exécutera.
    terraform apply: Applique les modifications spécifiées dans votre configuration Terraform.
    terraform destroy: Détruit les ressources créées avec Terraform.