provider "aws" {
  region = "us-east-1"
}

# Définition du groupe de sécurité
resource "aws_security_group" "mysg" {
  name_prefix = "mysg"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Accès public
  }
}
resource "aws_instance" "myec" {
  ami = "ami-0fc5d935ebf8bc3bc" # ID de l'AMI Ubuntu
  instance_type = "t2.micro" # Type d'instance
  tags = {
    Name = "myec"
  }
  # Attachement du groupe de sécurité
  vpc_security_group_ids = [aws_security_group.mysg.id]
   
  # Script d'initialisation
  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y apache2
              echo "Hello Terraform" | sudo tee /var/www/html/index.html
              EOF
}